import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { } from 'rxjs';
import { Answer } from 'src/models/answer';
import { Question } from 'src/models/question';

@Injectable({ providedIn: 'root' })
export class QuestionnaireService {

  constructor() { }

  // TODO:
  // Use RxJS and the Angular HTTP client.

  // Functionality #1: 
  // Get the questions.
  // Http GET https://printer.ydrogiosonline.gr/api/interviews/questions
  // queryParams: apiVersion=1.0
  // returns: Question[]

  // Functionality #2:
  // Get the answers for a given questionId (guid/string).
  // Http GET https://printer.ydrogiosonline.gr/api/interviews/answers
  // routeParams: questionId
  // queryParams: apiVersion=1.0
  // returns Answer[]
}
